# configuration applied to all sampa chips
# address data
# you can add comment followed by #
# even at the end of a line
# by default values are interpreted as decimal values
# but a 'h or 'b prefix allows to write hex or binary values
# examples:
# 'haa 'b0001
# 'b101 14 # you can add a comment here
# empty lines have no incidence
# lines beginning with a # are not seen
#"zero suppress, no cluster sum
 
'h0e 'h5 #software reset at the end

'h0d 'h20 # trigger mode  + power save
'h12 'h11 # disable 100 ohm and only 1 link
'h07 'he7 # TWLEN 3E7
'h08 'h03 # TWLEN 3E7
'h0b 'he6 # ACQEND 3E6
'h0c 'h03 # ACQEND 3E6

'h06 192 # set pre trigger delay to 19.2 us


#'h13 'h00 # set output power to norm
#'h13 'h55 # set output power to low
'h13 'haa # set output power to max!
'h24 'hff
'h25 'hff
'h26 'hff
'h27 'hff

# DPCFG 200
# set chrgadd
'h17 'h18
# set data 'h0200
'h18 'h00
'h19 'h02
# set for all channels
'h1a 'h60

# ZSCFG 7F
# set chrgadd
'h17 'h0B
# set data 'h0002
'h18 'h7F
'h19 'h00
# set for all channels
'h1a 'h60

# ZSTHR 40
# set chrgadd
'h17 'h09
# set data 'h000C
'h18 'h40
'h19 'h00
# set for all channels
'h1a 'h60

'h0e 'h5 #software reset at the end
