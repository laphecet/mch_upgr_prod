# -*- coding: utf-8 -*-

import subprocess
from CGrorc import *
from CI2c import *

import random
import time

class CSca:
	"""Management of the SCA companion chip"""

	def __init__(self,hdlGrorc):
		self._hdlGrorc=hdlGrorc
		self._bLogSca=hdlGrorc._bLogSca
		#self._hdlGrorc._initSca() # moved into CMch
		self._i2c=[]
		for idxI2C in range (0,16):
			self._i2c.append(CI2c(self,idxI2C))



	def _enable(self,cspi='0',cgpio='0',stri2c='0000000000000000',cjtag='0',cadc='0',cdac='0'):
		# value is made of enables for
		# 0 : reserved
		# 1 : enable SPI
		# 2 : enable GPIO
		# 3 : enable I2C channel 0
		# ... ... ...
		# 18: enable I2C channel 15
		# 19: enable JTAG
		# 20: enable ADC
		# 21: enable DAC
		# ... see below


		# enable interfaces with SCA Control Register B (bits 7:0)
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# * Enable-Disable I2C[0 to 4] and GPIO (SCA register B)")
		val="%.8X" % int('%s%s%s%s%s%s%s%s'%(stri2c[-5] ,stri2c[-4], stri2c[-3], stri2c[-2], stri2c[-1], cgpio,      cspi,      '0'         ),2)
		len='01'
		cmd='02' # control register in the Sca
		self._writeReg('00',cmd,val,len)
		#self._writeReg('00',cmd,'00000004',len)
		# enable interfaces with SCA Control Register C (bits 15:8)
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# * Enable-Disable I2C[5 to 8] (SCA register C)")
		val="%.8X" % int('%s%s%s%s%s%s%s%s'%(stri2c[-13],stri2c[-12],stri2c[-11],stri2c[-10],stri2c[-9], stri2c[-8], stri2c[-7], stri2c[-6] ),2)
		cmd='04' # control register in the Sca
		self._writeReg('00',cmd,val,len)
		# enable interfaces with SCA Control Register D
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# * Enable-Disable ADC (SCA register D)")
		val="%.8X" % int('%s%s%s%s%s%s%s%s'%('0',        '0',        '0',          cdac,       cadc,      stri2c[-16],stri2c[-15],stri2c[-14]),2)
		cmd='06' # control register in the Sca
		self._writeReg('00',cmd,val,len)

		if(cgpio=='1'):
			#set gpio all  out
			self._writeReg('02','20','ffffffff','04')
			#set a random value to leds ;)
			self._writeReg2('02','10','%.8X'%(random.randint(1,7)),'04')
		else:
			self._writeReg('02','20','0','04')


	def _writeReg2(self,cChan,cCmd,cVal,cLen):
		#       LEN is the message length (hexadecimal, example 01)
		#       CMD is the message command word (hexadecimal, example 02)
		#       CHAN is the targeted channel number (hexadecimal, example 00)
		cTr='%.2X'%(random.randint(1, 254))
		#print("Transaction Nb %s : Write Channel %s, command %s, value %s, (length %s)"%(cTr,cChan,cCmd,cVal,cLen))
		self._write("%s%s%s%s"%(cChan,cTr,cLen,cCmd),cVal)
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# Write CHAN:%s TR:%s LEN:%s CMD:%s VAL:%s"%(cChan,cTr,cLen,cCmd,cVal))
			HdlLog.info("%s%s%s%s %s"%(cChan,cTr,cLen,cCmd,cVal))
		#self._exec()
		return()

	def _writeReg(self,cChan,cCmd,cVal,cLen):
		#       LEN is the message length (hexadecimal, example 01)
		#       CMD is the message command word (hexadecimal, example 02)
		#       CHAN is the targeted channel number (hexadecimal, example 00)
		cTr='%.2X'%(random.randint(1, 254))
		#print("Transaction Nb %s : Write Channel %s, command %s, value %s, (length %s)"%(cTr,cChan,cCmd,cVal,cLen))
		lNewVal=cVal[6:8]+cVal[4:6]+cVal[2:4]+cVal[0:2]
		self._write("%s%s%s%s"%(cChan,cTr,cLen,cCmd),lNewVal)
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# Write CHAN:%s TR:%s LEN:%s CMD:%s VAL:%s"%(cChan,cTr,cLen,cCmd,cVal))
			HdlLog.info("%s%s%s%s %s"%(cChan,cTr,cLen,cCmd,cVal))
		#self._exec()
		return()

	def _readReg(self,cChan,cCmd,cLen):
		#       LEN is the message length (hexadecimal, example 01)
		#       CMD is the message command word (hexadecimal, example 02)
		#       CHAN is the targeted channel number (hexadecimal, example 00)
		cTr='%.2X'%(random.randint(1, 254))
#		print("Transaction Nb %s : Read Channel %s, command %s (length %s)"%(cTr,cChan,cCmd,cLen))
		self._write("%s%s%s%s"%(cChan,cTr,cLen,cCmd),'00000000')
		#self._exec()
		strAns=self._read()
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# Read CHAN:%s TR:%s LEN:%s CMD:%s ExpectedVAL:%s"%(cChan,cTr,cLen,cCmd,strAns))
			HdlLog.info("%s%s%s%s %s"%(cChan,cTr,cLen,cCmd,strAns))
		return(strAns)

	def _write(self,strVal1,strVal2):
		self._hdlGrorc._write(strVal1,strVal2)

	def _read(self):
		return(self._hdlGrorc._read())

	def _exec(self):
		self._hdlGrorc._exec()
