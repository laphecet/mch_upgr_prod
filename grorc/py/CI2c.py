# -*- coding: utf-8 -*-

import subprocess
from CGrorc import *
from CColor import *
import time
import random

class CI2c:
	"""Methods for the I²C commands"""

	def __init__(self,hdlSca,nIdx):
		self._nIdx=nIdx
		self._hdlSca=hdlSca
		self._bLogSca=hdlSca._bLogSca
		self._chan="%.2X"%(self._nIdx+3)
		self._nConstNbRetry=2 # MAX Number Retries for I2C access
		self._nNbRetry=self._nConstNbRetry # MAX Number Retries for I2C access (modified on the fly, so don't  put a value here)
		self._nFreq=1	

	def _config(self,nFreq,nBytes):
		# nFreq is defined as 0:100kHz, 1:200kHz, 2:400kHz, 3:1MHz
		# nBytes is a 0 to 15 decimal number (number of bytes)
		self._nFreq=nFreq
		self._nBytes=nBytes
		value='%.8X'%(self._nBytes*4+self._nFreq)
		#value='%.8X'%(self._nBytes*4+self._nFreq+128)
		#,strFreq)
		cmd='30' # Write Control Register in te SCA is 0x30
		len='02'
		self._hdlSca._writeReg(self._chan,cmd,value,len)

	def _interpretStatus(self,strAns,bAckExpected=1,strComment=''):
                #strAns=strAns.splitlines()[-1]
		#strAns=strAns[-9:-7]
		strAns1=strAns[0:2]
		bits=bin(int(strAns1,16))[2:].zfill(8)
		if(bits[-2-1]=='1'):
			return(1)
		elif(bits[-6-1]=='1'):
			if(bAckExpected==1):
				if(strComment!=''):
					print('%sWARNING%s: No I²C ACK for %s'%(CColor.YELLOW,CColor.END,strComment))
				return(0)
			else:
				return(1)
		elif(bits[-3-1]=='1'):
			if(strComment!=''):
				print('%sERROR%s: I²C SDA line is low for %s'%(CColor.RED,CColor.END,strComment))
			return(-1)
		elif(bits[-5-1]=='1'):
			if(strComment!=''):
				print('%sERROR%s: I²C Error, please reset'%(CColor.RED,CColor.END))
			return(0)
		else:
			if(strComment!=''):
				print(strAns)
				if(strAns[0:8]=='00000000'):
					print('%sERROR%s: I²C Error (SCA configured?)'%(CColor.RED,CColor.END))
				else:
					print('%sWARNING%s: I²C Status 0x00 for %s'%(CColor.YELLOW,CColor.END,strComment))
			return(0)
		
	def _byteWr(self,nI2CAddr,nByte):
		value='0000%.2X%.2X'%(nByte,nI2CAddr)
		cmd='82' # single byte write access in the SCA is 0x82
		len='02'
#		print 'Sending Byte 0x%s to I²C component %.2X on port %d' %(strByte,nI2CAddr,self._nIdx)
		for idxTry in range (0,self._nNbRetry):
			strAns=self._hdlSca._writeReg(self._chan,cmd,value,len)
			strAns=self._hdlSca._read()
			#strAns=self._hdlSca._writeReg(self._chan,'11',value,len)
			#strAns=self._hdlSca._readReg(self._chan,'11',len)
			status=self._interpretStatus(strAns)
			if(status==1)or(status==-1):
				break
		#strAns=strAns.splitlines()[-1]
		#strAns=strAns[-9:-7] #[-2:]
		status=self._interpretStatus(strAns,1,'write of 0x%.2X to device 0x%.2X on port %d'%(nByte,nI2CAddr,self._nIdx))
		strAns=strAns[0:2]
		strAns='%s (%d)'%(strAns,idxTry)
		#print(strAns)
		return(strAns)

	def _byteWrExt(self,nI2CAddrH,nI2CAddrL,nByte,bRetry):
		if(bRetry<0):
			self._nNbRetry=self._nConstNbRetry
		else:
			self._nNbRetry=1
		value='00%.2X%.2X%.2X'%(nByte,nI2CAddrL,nI2CAddrH)
		cmd='8A' # single byte write access in the SCA is 0x8A (10 bit addressing)
		len='03'
##		print 'Sending Byte 0x%.2X to I²C component %.2X-%.2X on port %d' %(nByte,nI2CAddrH,nI2CAddrL,self._nIdx)
		for idxTry in range (0,self._nNbRetry):
			strAns=self._hdlSca._writeReg(self._chan,cmd,value,len)
			strAns=self._hdlSca._read()
			status=self._interpretStatus(strAns,bRetry)
			#subprocess.Popen("usleep 400",shell=True,stdout=subprocess.PIPE).stdout.read()
			
			#if(bRetry!=0)and((strAns=="04")or(strAns=="00"))and(status==1): # check if what is written is equal to what we wanted to write...
			if(bRetry!=0)and(((int(strAns[1],16)&4)==4)or(strAns=="00"))and(status==1): # check if what is written is equal to what we wanted to write...
				strAns1=self._hdlSca._writeReg(self._chan,'8E',value,len)
				strAns1=self._hdlSca._read()
				
				nStatus=self._interpretStatus(strAns1,1)
				nRbkRetrys=0
				while(nStatus!=1):
					if(nRbkRetrys>10):
						print('%sERROR%s: Cannot Read back after write of 0x%.2X to device 0x%.2X:%.2X on port %d'%(CColor.RED,CColor.END,nByte,nI2CAddrH,nI2CAddrL,self._nIdx))
						#status=-1
						strAns=strAns1
						break
					nRbkRetrys=nRbkRetrys+1
					print('%sWARNING%s: Read back error (retrying) for write of 0x%.2X to device 0x%.2X:%.2X on port %d'%(CColor.YELLOW,CColor.END,nByte,nI2CAddrH,nI2CAddrL,self._nIdx))
					strAns1=self._hdlSca._writeReg(self._chan,'8E',value,len)
					strAns1=self._hdlSca._read()
					nStatus=self._interpretStatus(strAns1,1)
				
				if(nStatus==1):
					#print('%d==%d'%(nByte,int(strAns1[2:4],16)))
					if(nByte!=int(strAns1[2:4],16)):
						print('%sERROR%s: new value %d different from sampa registered value %d after writing 0x%.2X to device 0x%.2X:%.2X on port %d'%(CColor.RED,CColor.END,nByte,int(strAns1[2:4],16),nByte,nI2CAddrH,nI2CAddrL,self._nIdx))
						#status=-1
						strAns=strAns1
					
					
					else:
						#print('*newval %d == sampaval %d'%(nByte,int(strAns1[2:4],16)))
						status=1
						strAns='04'

			
			
			if(status==1)or(status==-1)or(bRetry==0):
				break
			#subprocess.Popen("usleep 100",shell=True,stdout=subprocess.PIPE).stdout.read()
		status=self._interpretStatus(strAns,bRetry,'write of 0x%.2X to device 0x%.2X:%.2X on port %d'%(nByte,nI2CAddrH,nI2CAddrL,self._nIdx))
		#strAns=strAns.splitlines()[-1]
		#print(strAns)
		#strAns=strAns[-9:-7]
		strAns=strAns[0:2]
		strAns='%s (%d)'%(strAns,idxTry)
		return(strAns)

	def _byteRd(self,nI2CAddr):
		value='%.8X'%(nI2CAddr)
		cmd='86' # single byte read access in the SCA is 0x86
		len='02'
#		print 'Receiving 1 Byte from I²C component %.2X on port %d' %(nI2CAddr,self._nIdx)
		# here we are obliged to use a writeReg Method, as we must give the I2C address
		strAns=self._hdlSca._writeReg(self._chan,cmd,value,len)
		strAns=self._hdlSca._read()
		#strAns=strAns.splitlines()[-1]
		strAns=strAns[2:4]
		return(strAns)

	def _byteRdExt(self,nI2CAddrH,nI2CAddrL):
		value='%.6X%.2X'%(nI2CAddrL,nI2CAddrH)
		cmd='8E' # single byte read access in the SCA is 0x8E (10 bit addressing)
		len='02'
#		print 'Receiving 1 Byte from I²C component %.2X-%.2X on port %d' %(nI2CAddrH,nI2CAddrL,self._nIdx)
		# here we are obliged to use a writeReg Method, as we must give the I2C address
		strAns=self._hdlSca._writeReg(self._chan,cmd,value,len)
		strAns=self._hdlSca._read()
		#strAns=strAns.splitlines()[-1]
		strAns=strAns[2:4]
		return(strAns)

	def _mbyteRd(self,nI2CAddr,nSize):
		# set number of bytes
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# ** MultiByte Read sequence")
			HdlLog.info("# * Set Number of bytes to read")
		self._config(self._nFreq,nSize)

		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# * Start a multi-Byte I²C transaction")
		value='000000%.2X'%(nI2CAddr)
		cmd='DE' # multi byte read access in the SCA is 0xDE
		len='02'
		strAns=self._hdlSca._writeReg(self._chan,cmd,value,len)
		strAns=self._hdlSca._read()
#		print 'Receiving %d Bytes from I²C component %.2X on port %d' %(nSize,nI2CAddr,self._nIdx)
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# * Read the different bytes stored in the SCA registers")
		strAns=''
		for idxByte in range (0,nSize,4):
			cmd='71'
			cmd='%.2X'%(int(cmd,16)-4*idxByte)
			lstrAns=self._hdlSca._readReg(self._chan,cmd,len)
			#lstrAns=self._hdlSca._writeReg(self._chan,cmd,value,len)
			#lstrAns=lstrAns.splitlines()[-1]
			#lstrAns=lstrAns[-2:]+lstrAns[-4:-2]+lstrAns[-7:-5]+lstrAns[-9:-7]
			lstrAns=lstrAns[6:8]+lstrAns[4:6]+lstrAns[2:4]+lstrAns[0:2]
			strAns='%s%s'%(lstrAns,strAns)
		strAns=strAns[-2*nSize:]
		return(strAns)
