# -*- coding: utf-8 -*-

from CSolar import *
from CColor import *

class CSampa:
	"""Methods to talk with the SAMPA chip"""
	def __init__(self,nIdxGrorc=-1,nIdxSolar=-1,nPort=-1,nPos=-1,nIdxSampa=-1,hdlSolar=-1,strConfigFile=""):
		if hdlSolar==-1:
			self._empty=1
		else:
			self._empty=0
		self._nIdxGrorc=nIdxGrorc
		self._nIdxSolar=nIdxSolar
		self._nPos=nPos # position on flex
		self._nPort=nPort
		self._nIdx=nIdxSampa
		self._hdlSolar=hdlSolar
		self._bLogSca=hdlSolar._bLogSca
		self._nChipI2cAddrH=0
		self._nChipI2cAddrL=0
		self._strConfigFile=strConfigFile

	def located(self,nSampaNb,nPos,nPort,nGrorcPort,nGrorcNb):
		if self._empty==1:
			return(0)
		if self._nPort==nPort and self._nPos==nPos and self._nIdx==nSampaNb and self._nIdxSolar==nGrorcPort and self._nIdxGrorc==nGrorcNb:
			return(1)
		else:
			return(0)

	def _calcAddr(self,nRegAddr):
		nPos=2*self._nPos+self._nIdx
		nChipI2cAddrH=nPos/4
		nChipI2cAddrL=nPos-(nPos/4)*4
		nChipI2cAddrH=nChipI2cAddrH+int('1111000',2)
		nChipI2cAddrL=nChipI2cAddrL*64+nRegAddr
		self._nChipI2cAddrH=nChipI2cAddrH
		self._nChipI2cAddrL=nChipI2cAddrL
		#print('%.2X %.2X'%(nChipI2cAddrH,nChipI2cAddrL))
		#print(self.__repr__())

		#print('calcaddr %d %d %d'%(self._nPos,nChipI2cAddrH,nChipI2cAddrL))

	def __repr__(self):
		return('Sampa Chip number %d on I2Cbus %d of SOLAR %d on GRORC %d'%(2*self._nPos+self._nIdx,self._nPort,self._nIdxSolar,self._nIdxGrorc))

	#def readRegister(self,nAddress,nSize)
	
	def _readValue(self,string):
		if "'h" in string:
			return(int(string[2:],16))
		elif "'b" in string:
			return(int(string[2:],2))
		else:
			return(int(string))

	def configure(self):
		# test if SAMPA is responding
		if(self.testi2cwrite()==False):
			print('%sERROR%s:chip could not be found: %s'%(CColor.RED,CColor.END,self.__repr__()))
			return


		# configuration itself
		strFileName=self._strConfigFile
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# ==============================================================")
			HdlLog.info("# *** Configure %s"%(self.__repr__()))
		f=open(strFileName,'r')
		lbStatus=True
		for strLine in f:
			strLine=strLine.strip()
			if(len(strLine)>0):
				if(strLine[0]!='#'):
					idxComment=strLine.find('#')
					if(idxComment>0):
						strLine=strLine[:idxComment]
					idxSpace=strLine.find(' ')
					nAddr=self._readValue(strLine[:idxSpace])
					nDat=self._readValue(strLine[idxSpace+1:])
					#print('addr:%.2X data:%.2X %s'%(nAddr,nDat,self.__repr__()))
					#print('writing addr:%.2X data:%.2X'%(nAddr,nDat))
					if(self._bLogSca==True):
						HdlLog=logging.getLogger('scalog')
						HdlLog.info("# * Write Sampa Register Address:0x%X Data:0x%X"%(nAddr,nDat))
					strAns=self.write(nAddr,nDat)
					#subprocess.Popen("usleep 100000",shell=True,stdout=subprocess.PIPE).stdout.read()
					if "(0)" not in strAns:
						print('   addr:%.2X data:%.2X [%s] %s'%(nAddr,nDat,strAns,self.__repr__()))
					if ((int(strAns[1],16)&4)!=4): 
						# better than [if "04" not in strAns:], I prefer testing if I have the bit "success"
						#if ("40" in strAns) and  (lbStatus==False):
						lbStatus=False
					#elif ((int(strAns[1],16)&8)!=8): 
					#	print('LEVERRaddr:%.2X data:%.2X [%s] %s'%(nAddr,nDat,strAns,self.__repr__()))
					#else:
					#	print('   OK addr:%.2X data:%.2X [%s] %s'%(nAddr,nDat,strAns,self.__repr__()))
				else:
					if(len(strLine)>1) and (strLine[1]=='"'):
						print('%s   *%s*%s'%(CColor.CYAN,strLine[2:],CColor.END))
						if(self._bLogSca==True):
							HdlLog=logging.getLogger('scalog')
							HdlLog.info("# * %s"%(strLine[2:]))
						


		if(lbStatus==True):
			print('%sSUCCESS%s:chip successfully configured: %s'%(CColor.GREEN,CColor.END,self.__repr__()))
		else:
			print('%sERROR%s:chip could not be configured: %s'%(CColor.RED,CColor.END,self.__repr__()))


	def testi2cwrite(self):
		nAddress=0x24
		nByte=0xff
		lbRetry=-1
		self._calcAddr(nAddress)
		strAns=self._hdlSolar.i2c(self._nPort)._byteWrExt(self._nChipI2cAddrH,self._nChipI2cAddrL,nByte,lbRetry)
		if ((int(strAns[1],16)&4)!=4): 
			return(False)
		else:
			return(True)


	def write(self,nAddress,nByte):
		if self._empty==1:
			return("ERROR: Sampa doesn't exist")

		if(nAddress==int('0e',16))and(nByte==5):
			lbRetry=0
		else:
			lbRetry=1

		#nChipPos=self._nPos*2+self._nIdx
		self._calcAddr(nAddress)
		#strAns1=self._hdlSolar.i2c(self._nPort)._byteRdExt(self._nChipI2cAddrH,self._nChipI2cAddrL)
		strAns=self._hdlSolar.i2c(self._nPort)._byteWrExt(self._nChipI2cAddrH,self._nChipI2cAddrL,nByte,lbRetry)
		#strAns=self._hdlSolar.i2c(self._nPort)._byteRdExt(self._nChipI2cAddrH,self._nChipI2cAddrL)
		#print('%s'%(strAns))
		#print('%s->%s'%(strAns1,strAns))
		if(lbRetry==1):
			return(strAns)
		else:
			return('04 (0)')

	def read(self,nAddress):
		if self._empty==1:
			return("ERROR: Sampa doesn't exist")

		#nChipPos=self._nPos*2+self._nIdx
		self._calcAddr(nAddress)
		strAns=self._hdlSolar.i2c(self._nPort)._byteRdExt(self._nChipI2cAddrH,self._nChipI2cAddrL)
		#	strAns='to be or not to be...'
		print('Sampa Chip said: %s'%strAns)

		return(strAns)
