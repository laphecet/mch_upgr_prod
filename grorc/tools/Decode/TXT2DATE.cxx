#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <iostream>
#include <vector>

#include "decode.h"
#include "eventD7-106.h"

int outfd;


typedef struct _GBTWord
{
  uint32_t hhvalue,hlvalue,lhvalue,llvalue;
} GBTWord;


void saveEventBuffer( std::vector<GBTWord>& buffer )
{
  eventHeaderStruct header;
  header.eventMagic = EVENT_MAGIC_NUMBER;
  header.eventType = physicsEvent;
  header.eventGdcId = VOID_ID;
  header.eventHeadSize = sizeof( eventHeaderStruct );
  header.eventSize = sizeof( eventHeaderStruct ) + sizeof( equipmentHeaderStruct ) + 4*4*3 + buffer.size()*16;

  eventHeaderStruct ldc_header;
  ldc_header.eventHeadSize = sizeof( eventHeaderStruct );
  ldc_header.eventSize = sizeof( eventHeaderStruct ) + sizeof( equipmentHeaderStruct ) + 4*4*3 + buffer.size()*16;

  equipmentHeaderStruct eq_header;
  eq_header.equipmentSize = sizeof(equipmentHeaderStruct) + 4*4*3 + buffer.size()*16;
  //std::cout<<"equipmentSize = "<<sizeof(equipmentHeaderStruct)/4<<" + "<<""
  
  write(outfd, &header, sizeof(eventHeaderStruct));
  //write(outfd, &ldc_header, sizeof(eventHeaderStruct));
  write(outfd, &eq_header, sizeof(equipmentHeaderStruct));

  GBTWord wd;
  wd.hhvalue = 0x0;
  wd.hlvalue = 0x0;
  wd.lhvalue = 0x0;
  wd.llvalue = 0x1;

  write(outfd, &wd, sizeof(GBTWord));
  write(outfd, &wd, sizeof(GBTWord));
  write(outfd, &wd, sizeof(GBTWord));

  for(unsigned int i = 0; i < buffer.size(); i++) {
    //printf("%d: %.8X %.8X %.8X %.8X\n",i+1,
    //    buffer[i].hhvalue, buffer[i].hlvalue, buffer[i].lhvalue, buffer[i].llvalue);
    write(outfd, &(buffer[i]), sizeof(GBTWord));
  }

  wd.hhvalue = 0x1;
  wd.hlvalue = 0x2;
  wd.lhvalue = 0x3;
  wd.llvalue = 0x4;

  //write(outfd, &wd, sizeof(GBTWord));
}


int main(int argc, char** argv)
{
  FILE* fin = fopen(argv[argc-1],"r");
  printf("reading file \"%s\"\n", argv[argc-1]);
  int level = 0;
  bool GBTxfound = false;

  outfd = creat("/home/data/out.dat", S_IRWXU);

  gPrintLevel = 0;

  std::vector<GBTWord> buffer;
  char tstr[500];

  int n = 0;
  while(true) {
    if( !fgets(tstr,499,fin) ) break;
    uint32_t hhvalue,hlvalue,lhvalue,llvalue;
    sscanf(tstr,"%X %X %X %X", &hhvalue, &hlvalue, &lhvalue, &llvalue);
    //printf("%d: %.8X %.8X %.8X %.8X\n",n,
    //    hhvalue, hlvalue, lhvalue, llvalue);
    if(hhvalue>>16 != 0xDEF0) {

      if( GBTxfound ) {
        saveEventBuffer( buffer );
        buffer.clear();
        //return 0;
      }

      GBTxfound=false;
      if(gPrintLevel > 0) {
        printf("==================================================\n");
        printf("%d: %.8X %.8X %.8X %.8X\n",n,
            hhvalue, hlvalue, lhvalue, llvalue);
        printf("==================================================\n");
      }
    } else {
      GBTxfound = true;
      GBTWord gbtword;
      gbtword.hhvalue = hhvalue;
      gbtword.hlvalue = hlvalue;
      gbtword.lhvalue = lhvalue;
      gbtword.llvalue = llvalue;
      buffer.push_back( gbtword );
    }
    //printf("=========\n");
    n++;
  }
}
