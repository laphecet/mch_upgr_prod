#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <iostream>


int main(int argc, char** argv)
{
  FILE* fin = fopen(argv[1],"r");
  int level = 0;

  char tstr[500];

  bool GBTxfound = false;

  int32_t val_prev = -1;

  int n = 0;
  while(true) {
    if( !fgets(tstr,499,fin) ) break;
    uint32_t hhvalue,hlvalue,lhvalue,llvalue;
    sscanf(tstr,"%X %X %X %X", &hhvalue, &hlvalue, &lhvalue, &llvalue);
    //printf("%d: %.8X %.8X %.8X %.8X\n",n,
    //    hhvalue, hlvalue, lhvalue, llvalue);
    if(hhvalue>>16 != 0xDEF0) {
		GBTxfound=false;
		val_prev = -1;
		printf("==================================================\n");
		printf("%d: %.8X %.8X %.8X %.8X\n",n,
		hhvalue, hlvalue, lhvalue, llvalue);
		printf("==================================================\n");
      if( GBTxfound ) break;
    } else {
      GBTxfound = true;
      uint32_t bufpt[4] = {hhvalue, hlvalue, lhvalue, llvalue};
      uint32_t val1 = (bufpt[3]>>28) & 0xf;
      uint32_t val2 = (bufpt[2]>>0) & 0xffff;
      int32_t val = val1 + val2*16;
      //printf("bufpt[3]=%X  val1=%X val2=%X val=%x\n", 
      //     bufpt[3], val1, val2, val);
      printf("%d: %.8X %.8X %.8X %.8X -> counter=%X\n",n,
	     hhvalue, hlvalue, lhvalue, llvalue, val);
      if(val_prev>=0 && (val-val_prev)!=1) {
	printf("Wrong value in sequence!!!\n");
      }
      val_prev = val;
      if(val_prev == 0xFFFFF) val_prev = -1;
      //n++;
    }
    //printf("=========\n");
    n++;
  }

  return 0;
}
