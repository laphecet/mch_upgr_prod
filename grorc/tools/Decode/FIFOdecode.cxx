#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <iostream>

#include "decode.h"

extern "C" {
#include <rorc_ddl.h>
#include <rorc_lib.h>
}

rorc_pci_dev_t init(int minor, int channel);
__u32 read32bitword(rorc_pci_dev_t prorc, int addr);
void writeRegister(int minor, int channel, int addr, int val);
void readgbtword(int minor, int channel, __u32 *pvaluell, __u32 *pvaluelh, __u32 *pvaluehl, __u32 *pvaluehh);





rorc_pci_dev_t init(int minor, int channel)
{
  int ret;
  rorc_pci_dev_t prorc;


  //Open rorc
  ret = rorcQuickOpen(&prorc, minor, channel);
  if (ret && (ret != RORC_PCI_ERROR))
    exit (EXIT_FAILURE);

  if (pRorc(&prorc))
  {
    printf("PCI version 1 RORC is not supported.\n");
    exit (EXIT_FAILURE);
  }

  return prorc;
}



__u32 read32bitword(rorc_pci_dev_t prorc, int addr)
{
  int i;
  __u32 value;

  i = addr >> 2;

  value = dRorcReadReg(&prorc, i);
  //value = *(prorc.reg[0] + i);
  return(value);
}



void readgbtword(int minor, int channel, __u32 *pvaluell, __u32 *pvaluelh, __u32 *pvaluehl, __u32 *pvaluehh) {
  rorc_pci_dev_t prorc;
  //Open rorc
  //prorc = init(minor, channel);
  rorcQuickOpen(&prorc, minor, channel);
  //rorcMapChannel(&prorc, minor, channel);

  *pvaluehh=read32bitword(prorc, 0x114); //hh
  *pvaluehl=read32bitword(prorc, 0x110); //hl
  *pvaluelh=read32bitword(prorc, 0x10c); //lh
  *pvaluell=read32bitword(prorc, 0x108); //ll
  rorcClose(&prorc);
}



void writeRegister(int minor, int channel, int addr, int val) {
  rorc_pci_dev_t prorc;
  volatile unsigned *reg_addr; // long was before
  rorcMapChannel(&prorc, minor, channel);

  reg_addr = prorc.reg[0] + (addr >> 2);
  *reg_addr = val;

  rorcClose(&prorc);
  return;
}


void sendMID()
{
  // # this script works with the G-RORC firmware 3.16

  // # IDLE PATTERN
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a0 -v0x0300C030
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a4 -v0x00C0300C
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a8 -v0x0C03
  writeRegister(0,2,0x1a0,0x0300C030);
  writeRegister(0,2,0x1a4,0x00C0300C);
  writeRegister(0,2,0x1a8,0x0C03);

  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x80000000
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x0
  writeRegister(0,2,0x198,0x80000000);
  writeRegister(0,2,0x198,0x0);

  // # TRIGGER PATTERN

  // # 1 pulse trigger
  // #/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a0 -v0x0701C070
  // #/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a4 -v0x01C0701C
  // #/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a8 -v0x1C07

  // # long trigger
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a0 -v0x0F03C0F0
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a4 -v0x03C0F03C
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x1a8 -v0x3C0F
  writeRegister(0,2,0x1a0,0x0F03C0F0);
  writeRegister(0,2,0x1a4,0x03C0F03C);
  writeRegister(0,2,0x1a8,0x3C0F);

  // #The mapping of the 32 bits are

  // #s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
  // #s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
  // #s_gbt_val         <= s_reg(28);
  // #s_wr              <= s_reg(29);
  // #s_open_gate       <= s_reg(30);
  // #s_set_idle        <= s_reg(31);
  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
  writeRegister(0,2,0x198,0x60000001);

  // # wait for 103us=103000/4=25750=0x6496
  // #/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60609610
  // # The command above also starts the data taking
  // # if you need only to send the patterm without starting the data you can write
  // # /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x20000001

  // /date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x0
  writeRegister(0,2,0x198,0x0);
}



int main()
{
  gPrintLevel = 1;  // Global variable defined as extern in the others .cxx files

  DualSampa ds[40];

  int n = 0;
  while(true) {
    uint32_t hhvalue,hlvalue,lhvalue,llvalue;

    bool GBTxfound = false;

    for(int i = 0; i < 40; i++) {
      DualSampaInit( &(ds[i]) );
      ds[i].id = i;
    }

    // Reset FIFO
    writeRegister(0,2,0x104,0x12);
    writeRegister(0,2,0x104,0x2);
    usleep(1000);
    sendMID();
    usleep(1000);

   for(int i = 0; i < 8200; i++) {
      writeRegister(0,2,0x104,0x3);

      readgbtword(0,0,&llvalue,&lhvalue,&hlvalue,&hhvalue);
      n++; if( (n%1000) == 0 ) printf("%d\n",n);

      //printf("%d: %.8X %.8X %.8X %.8X\n",n,hhvalue, hlvalue, lhvalue, llvalue); //continue;
      if(hhvalue>>16 != 0xDEF0) {
        printf("==================================================\n");
        printf("%d: %.8X %.8X %.8X %.8X\n",n,
            hhvalue, hlvalue, lhvalue, llvalue);
        printf("==================================================\n");
        if( GBTxfound ) break;
      } else {
        GBTxfound = true;
        uint32_t bufpt[4] = {hhvalue, hlvalue, lhvalue, llvalue};
        uint32_t data2bits[40];

        DecodeGBTWord(bufpt, data2bits);
        for(int i = 0; i < 1; i++) {
          Add1BitOfData( data2bits[i]&0x1, &(ds[i]) );
          Add1BitOfData( (data2bits[i]>>1)&0x1, &(ds[i]) );
        }
      }
      //printf("=========\n");
    }
    //getchar();
  }
}
