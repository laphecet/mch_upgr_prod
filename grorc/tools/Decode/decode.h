#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "sampa_header.h"

extern int gPrintLevel;
extern int gPattern;
extern int gNbErrors;
extern int gNbWarnings;



enum DualSampaStatus {
    notSynchronized              = 1,
    synchronized                 = 2,
    headerToRead                 = 3,
    sizeToRead                   = 4,
    timeToRead                   = 5,
    dataToRead                   = 6,
    chargeToRead                 = 7,
    OK                           = 8   // Data block filled (over a time window)
};


struct DualSampa
{
  int id;
  DualSampaStatus status;            // Status during the data filling
  uint64_t data;                // curent data
  int bit;                           // current position
  uint64_t powerMultiplier;     // power to convert to move bits
  int nsyn2Bits;                     // Nb of words waiting synchronization
  Sampa::SampaHeaderStruct header;   // current channel header
  uint32_t csize, ctime, cid;
  int chan_addr[2];
  uint64_t packetsize;
  int nbHit; // incremented each time a header packet is received for this card
  int nbHitChan[64]; // incremented each time a header packet for a given packet is received for this card
  int ndata[2][32];
  int nclus[2][32];
  double pedestal[2][32], noise[2][32];
};


void DualSampaInit(DualSampa* ds);
void DualSampaReset(DualSampa* ds);
int CheckDataParity(uint64_t data);
void HammingDecode(unsigned int buffer[2], bool& error, bool& uncorrectable, bool fix_data);
void DecodeGBTWord(uint32_t* bufpt, uint32_t* data);
void Add1BitOfData(uint64_t gbtdata, DualSampa* ds);
