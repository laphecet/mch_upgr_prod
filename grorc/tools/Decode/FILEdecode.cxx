#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <iostream>

#include "decode.h"


int main(int argc, char** argv)
{
  FILE* fin = fopen(argv[argc-1],"r");
  printf("reading file \"%s\"\n", argv[argc-1]);
  int level = 0;
  gPrintLevel = level;  // Global variable defined as extern in the others .cxx files
  gPattern = 0;
  gNbErrors = 0;
  gNbWarnings = 0;
  int boardnum = -1;
  bool print_noise = false;


  // decode the input line
  for (int i = 1; i < argc; i++) { // argument 0 is the executable name
    char *arg;
    
    arg =  argv[i];
    if (arg[0] != '-') continue;
    switch (arg[1]) {
    case 'p' :
      i++;
      //	  InputFile = argv[i];
      gPattern=strtol(argv[i], NULL, 0);
      break;
    case 'd' :
      i++;
      gPrintLevel=atoi(argv[i]);
      break;
    case 'n' :
      i++;
      print_noise=atoi(argv[i]);
      break;
    case 'b' :
      i++;
      boardnum=atoi(argv[i]);
      break;
    }
  }
  
  int NbCardStart = 0;
  int NbCardEnd = 39;
  if(boardnum >= 0) {
  	  NbCardStart = boardnum;
  	  NbCardEnd = NbCardStart;
  }

  char tstr[500];
  DualSampa ds[40];

  for(int i = 0; i < 40; i++) {
    DualSampaInit( &(ds[i]) );
    ds[i].id = i;
    ds[i].nbHit = -1;
	for(int j=0;j<64;j++) {
		ds[i].nbHitChan[j]=0;
	}
  }

  bool GBTxfound = false;

  int n = 0;
  for(int i = 0; i < 0; i++) {
    if( !fgets(tstr,499,fin) ) break;
  }
  while(true) {
    if( !fgets(tstr,499,fin) ) break;
    uint32_t hhvalue,hlvalue,lhvalue,llvalue;
    sscanf(tstr,"%X %X %X %X", &hhvalue, &hlvalue, &lhvalue, &llvalue);
    //printf("%d: %.8X %.8X %.8X %.8X\n",n,
    //    hhvalue, hlvalue, lhvalue, llvalue);
    if(hhvalue>>16 != 0xDEF0) {
		GBTxfound=false;
  		for(int i = 0; i < 40; i++) {
		    DualSampaReset( &(ds[i]) );
		  }
		if(gPrintLevel > 0) {
		  printf("==================================================\n");
		  printf("%d: %.8X %.8X %.8X %.8X\n",n,
			 hhvalue, hlvalue, lhvalue, llvalue);
		  printf("==================================================\n");
		}
      if( GBTxfound ) break;
    } else {
      GBTxfound = true;
      uint32_t bufpt[4] = {hhvalue, hlvalue, lhvalue, llvalue};
      uint32_t data2bits[40];
      DecodeGBTWord(bufpt, data2bits);

      
      //for(int i = dsboard; i <= dsboard; i++) {
      for(int i = NbCardStart; i <= NbCardEnd ; i++) {
        Add1BitOfData( data2bits[i]&0x1, &(ds[i]) );
        Add1BitOfData( (data2bits[i]>>1)&0x1, &(ds[i]) );
      }
    }
    //printf("=========\n");
    n++;
  }

  if( print_noise ) {
    for(int i = 0; i< 40; i++) {
      for(int j = 0; j < 2; j++) {
	for(int k = 0; k < 32; k++) {
	  if( ds[i].ndata[j][k] > 0 ) {
	    float ped = (float)(ds[i].pedestal[j][k]/ds[i].ndata[j][k]);
	    float noise = (float)sqrt( ds[i].noise[j][k]/ds[i].ndata[j][k] - ped*ped );
	    printf("%2d %d %2d  %0.3f  %0.3f  %d %d\n",i, j, k, ped, noise, ds[i].ndata[j][k], ds[i].nclus[j][k]);
	  }
	}
      }
    }
  }

  for(int i = 0; i< 50; i++) 
    printf("-");
  printf("\n");
  printf("ERRORS: %.5d\t WARNINGS: %.5d\n",gNbErrors,gNbWarnings);
  for(int i = 0; i< 50; i++)
    printf("-");
  printf("\n");
  for(int i = 0; i< 8; i++) {
  	for(int j = 0; j< 5; j++) {
		int k=5*i+j;
		for(int l=1;l>=0;l--) {
			uint64_t hits=0;
			for(int m = 31; m>=0; m--) { 
				hits=(hits<<1)+(ds[k].nbHitChan[m+32*l]>0);
				//printf("%.2d - %.1d %.16X\n",jj,ds[i].nbHitChan[j+32*k],hits);
			}
			printf("%.8X",hits);
		}
		printf(" ");
	}
	printf("\n");
  }
  for(int i = 0; i< 50; i++)
    printf("-");
  printf("\n");
  for(int i = 0; i< 8; i++) {
	printf("{J%d}:: ",i+1);
  	for(int j = 0; j< 5; j++) { 
		int k=5*i+j;
		if(ds[k].nbHit>=0)
			printf("[%.2d]%.5d\t",k,ds[k].nbHit);
		else
			printf("[%.2d]-----\t",k);
	}
	printf("\n");
  }
}
