# -*- coding: utf-8 -*-

import smbus
from CColor import *

import random

class CTestSolarI2c:
	"""Class to test the I2C lines on SOLAR"""

	def __init__(self,bDebug=False):
		self.bDebug=bDebug 
		self.bStatus=True 
		self.strAns=""


		if(self.bDebug==False):
			self._bus=smbus.SMBus(1) # 0 = /dev/i2c-0 (port I2C0) ; 1...for I2C1
			self._glibAddr=0x3c
			try:
				self._bus.write_byte_data(self._glibAddr,0,0) # set ptr at first address
			except IOError as err:
				errno, strerror = err.args
				print("I/O error({0}): {1}".format(errno, strerror))
				self.bStatus=False 

	def __repr__(self):
		return(str(self.bStatus))

	def __str__(self):
		return(str(self.__dict__))

	def _writeScaReg(self,channel,length,command,datahh,datahl,datalh,datall,bVerbose=True):
		self._bus.write_byte_data(self._glibAddr,8,channel)
		self._bus.write_byte_data(self._glibAddr,9,random.randint(1,254)) # id
		self._bus.write_byte_data(self._glibAddr,10,length)
		self._bus.write_byte_data(self._glibAddr,11,command)

		self._bus.write_byte_data(self._glibAddr,12,datahh)
		self._bus.write_byte_data(self._glibAddr,13,datahl)
		self._bus.write_byte_data(self._glibAddr,14,datalh)
		self._bus.write_byte_data(self._glibAddr,15,datall)
		
		self._bus.write_byte_data(self._glibAddr,7,4) # run
		self._bus.write_byte_data(self._glibAddr,7,0) # idle

		if(bVerbose==True):
			for i in range (0,3):
				hh=self._bus.read_byte_data(self._glibAddr,32+4+i*4+0)
				hl=self._bus.read_byte_data(self._glibAddr,32+4+i*4+1)
				lh=self._bus.read_byte_data(self._glibAddr,32+4+i*4+2)
				ll=self._bus.read_byte_data(self._glibAddr,32+4+i*4+3)
				print("%.2d: %.2X%.2X%.2X%.2X"%(32+4+i*4,hh,hl,lh,ll))

	def _readScaReg(self,channel,length,command,bVerbose=True):
		self._bus.write_byte_data(self._glibAddr,8,channel)
		self._bus.write_byte_data(self._glibAddr,9,random.randint(1,254)) # id
		self._bus.write_byte_data(self._glibAddr,10,length)
		self._bus.write_byte_data(self._glibAddr,11,command)

		self._bus.write_byte_data(self._glibAddr,7,4) # run
		self._bus.write_byte_data(self._glibAddr,7,0) # idle

		if(bVerbose==True):
			for i in range (2,3):
				hh=self._bus.read_byte_data(self._glibAddr,32+4+i*4+0)
				hl=self._bus.read_byte_data(self._glibAddr,32+4+i*4+1)
				lh=self._bus.read_byte_data(self._glibAddr,32+4+i*4+2)
				ll=self._bus.read_byte_data(self._glibAddr,32+4+i*4+3)
				print("%.2d: %.2X%.2X%.2X%.2X"%(32+4+i*4,hh,hl,lh,ll))
				self.strAns="%s%.2X%.2X%.2X%.2X"%(self.strAns,hh,hl,lh,ll)

	def _enI2CPorts(self):
		print("enable I2C interfaces 0 to 4")
		self._writeScaReg(0,1,0x02,0xf8,0,0,0,False)
		print("enable I2C interfaces 5 to 8")
		self._writeScaReg(0,1,0x04,0xf,0,0,0,False)
		for i in range (0,9):
			self._setI2cDefault(i)

	def _setI2cDefault(self,nI2cPort):
		#print("set default i2c parameters for I2c %d"% nI2cPort)
		self._writeScaReg(nI2cPort+3,2,0x30,0x06,0,0,0,False) # for I2cPort n, channel is n+3 
		# value is nbytes*4+freq with (0: 100kHz, 1: 200kHz, 2: 400kHz, 3: 1MHz)
	
	def _setI2cMultiByte(self,nI2cPort):
		self._writeScaReg(nI2cPort+3,2,0x30,38,0,0,0,False) # for I2cPort n, channel is n+3 
		# value is nbytes*4+freq with (0: 100kHz, 1: 200kHz, 2: 400kHz, 3: 1MHz)

	def _readI2cMultiByte(self,nI2cPort,nI2cAddr):
		self._setI2cMultiByte(nI2cPort)
		self._writeScaReg(nI2cPort+3,2,0xDE,nI2cAddr,0,0,0,False) # for I2cPort n, channel is n+3 

		self._setI2cDefault(nI2cPort)

	def _readUniqueId(self):
		self._readDS28CM00(8) # chip is always on I2C port 8

	def _readDS28CM00(self,nI2cPort):
		self.strAns=""
		if(self.bDebug==False):
			# bytes read
			# address	 | data
			# 00		| device family code 0x70
			# 01		| number bits 0 to 7
			# 02		| ........... 8 to 15
			# ...		 |	...
			# 06		| number bits 40 to 47
			# 07		| CRC
			# 09		| control register
			nI2cAddr=int('50',16) # chip I2C address on bus is 0x50

			print("set uniqueID address to 1 on i2c link %d"%(nI2cPort))
			self._writeScaReg(nI2cPort+3,2,0x82,nI2cAddr,1,0,0,False) # for I2cPort n, channel is n+3 

			self._readI2cMultiByte(nI2cPort,nI2cAddr)
		
			self._readScaReg(nI2cPort+3,2,0x71) # for I2cPort n, channel is n+3 
			self._readScaReg(nI2cPort+3,2,0x61) # for I2cPort n, channel is n+3 
			#self._readScaReg(nI2cPort+3,2,0x51) # for I2cPort n, channel is n+3 
	
			# read chip identifier (shall be 0x70
			#strChipIDRead=self.i2c(nChipI2cPort)._byteRd(nChipI2CAddr) # read the byte
			#if strChipIDRead!='70':
			#	print('%sERROR%s: chip DS28CM00R could not be found!'%(CColor.BOLD,CColor.END))

			# read chip unique ID
			# set address 01 for link 8
			#self.i2c(nChipI2cPort)._byteWr(nChipI2CAddr,1)
			#return(self.i2c(nChipI2cPort)._mbyteRd(nChipI2CAddr,8)) # read the 8 bytes for the unique ID
		else:
			if(nI2cPort==8):
				self.strAns="4567CAFEFADE0123"
			else:
				self.strAns="ok"

	def init(self):
		self.strAns=""
		if(self.bDebug==False):
			# init sca
			self._bus.write_byte_data(self._glibAddr,7,1) # reset
			self._bus.write_byte_data(self._glibAddr,7,2) # connect
			self._bus.write_byte_data(self._glibAddr,7,1) # reset
			self._bus.write_byte_data(self._glibAddr,7,0) # idle

			self._enI2cPorts()

	def start(self,num):
		if (num==0):
			#print("Retrieving Unique ID")
			self._readUniqueId()
		elif (num<9):
			#print("Testing I2c port #%d"%num)
			self._readDS28CM00(num-1)
			rnderror=random.randint(1,12)
			if(rnderror>11):
				self.strAns="error"
		else:
			print("%sERROR:%s Unknown I2C test %d"%(CColor.RED,CColor.END,num))

	def read(self):
		return(self.strAns)

